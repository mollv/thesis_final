
# Evaluation of a physically based groundwater recharge parameterisation in the mesoscale Hydrologic Model (mHM)


### *Master Thesis*


This is the repository containing data and scripts used during my thesis in the CHS department during summer 2023 by me, Vincent Moll

All scripts created in the course of this project are published in this repository. I acknowledged external sources when used on top of each script. In most cases these were blog entries dealing with data structure or visualising topics.

There is a specific order in which scripts were run to deliver results as presented herein.

This order is:
1. read_data.R 
2. Brooks_Corey_coefficients.R
3. computation_of_RMSD.R
4. calibration_of_alpha.R
5. alpha_analysis.R
6. statistic.R
7. visualisation.R

At the begining of each script, a brief description of the purpose is given. Packages required are listed on top. The `install.packages()` command is only included for packages that are used for the first time when running scripts in the former presented order.


### Structure


This repository is structured as follows:

**data** contains all data used. 
The subdirectory *raw* contains data as obtained from various sources, *processed* data that was altered by e.g. the use of pedotransfer functions or so. *Reference_data_HICAM_GDM_DE3_v3_case3_crosseval* subdirectory contains experimental data that were used as reference while evaluating efficiency of the Brooks and Corey based parameterisation.

**results** contains results. Results include plots, R objects and `.txt` tables.
